Feature: WEB - Login/Seguridad 

    Scenario: AM-12 - Como un admin quisiera iniciar sesión en la aplicación web
        Given Ingreso a la pantalla de login
        When Ingreso con el usuario "gaston.genaud@arrowsoluciones.com.ar"
        Then retorna la pagina home

    Scenario: AM-9 - Como un admin quisiera visualizar la pantalla de login de la aplicación web
        Given Ingreso a la pantalla de login
        When Compruebo los criterios de aceptación
        Then Se visualiza el contenido de la pagina correctamente
    
    Scenario: AM-158 - Como un admin quisiera cerrar sesión
        Given Ingreso a la pantalla de login
        When Ingreso con el usuario "gaston.genaud@arrowsoluciones.com.ar"
        Then retorna la pagina home y cierro sesión