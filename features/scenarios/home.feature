Feature: WEB - Home

    Scenario: AM-55 - Como un admin quisiera ingresar al Home de la aplicación web
        Given Ingreso a la pagina de home
        When Accedo a las pantallas del menu del usuario "gaston.genaud@arrowsoluciones.com.ar"
        Then Se accede a las distintas pantallas y se visualiza su contenido del usuario "Gaston Genaud"
