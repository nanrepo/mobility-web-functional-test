/* eslint-disable import/no-extraneous-dependencies  */
import { When, Given, Then } from 'cucumber';
import { pages } from '../pages/pages';
import { request, gql } from 'graphql-request';
const dotenv = require('dotenv');
import {databaseQuery} from '../support/utils';

var email= "";
Given('Ingreso a la pantalla de register', async () => {
  await pages.register.navigate();
});

When('Completo el formulario de registro con el nombre {string} y la contraseña {string}', async (name, password) => {
  email = await pages.register.register(name, password);
});

When('clicks on content type {string}', async (type) => {
  await pages.home.setContentType(type);
});

Then('retorna la pagina register con el mail de validacion enviado', async () => {
  await pages.register.registerValidation();

});

