/* eslint-disable import/no-extraneous-dependencies  */
import { When, Given, Then } from 'cucumber';
import { pages } from '../pages/pages';

Given('Ingreso a la pagina de home', async () => {
  await pages.home.navigate();
});

When('Accedo a las pantallas del menu del usuario {string}', async (email) => {
  // Silenciado para fix de lib

  await pages.home.login(email);
  await pages.home.home(email);
});

Then('Se accede a las distintas pantallas y se visualiza su contenido del usuario {string}', async (fullName) => {
  await pages.home.homeValidate(fullName);
});

