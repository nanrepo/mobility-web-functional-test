import { Home } from './home.page';
import { Login } from './login.page';
import { Register } from './register.page';


export const pages = {
  login: new Login(),
  register: new Register(),
  home: new Home(),

};
