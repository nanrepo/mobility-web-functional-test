import { testController } from '../support/world'
import { select, makeid } from '../support/utils'
import { ClientFunction } from 'testcafe';

const password = '123456aA';
const dotenv = require('dotenv');
const email = makeid(5) + '@qanantech.com.ar';

export class Register {
  constructor () {
    dotenv.config();
    this.url = process.env.URL_HOST + `register`
  }

  nameImput  () {
    return select('div.MuiFormControl-root:nth-child(1) > div:nth-child(2) > input:nth-child(1)')
  }

  emailImput  () {
    return select('div.MuiFormControl-root:nth-child(2) > div:nth-child(2) > input:nth-child(1)')
  }

  passwordImput () {
    return select('div.MuiFormControl-root:nth-child(3) > div:nth-child(2) > input:nth-child(1)')
  }

  passwordValidateImput () {
    return select('div.MuiFormControl-root:nth-child(4) > div:nth-child(2) > input:nth-child(1)')
  }

  registerButton () {
    return select('button.MuiButtonBase-root:nth-child(5)');
  }

  passViewButton () {
    return select('div.MuiFormControl-root:nth-child(3) > div:nth-child(2) > div:nth-child(2) > button:nth-child(1)');
  }
  passValidateViewButton () {
    return select('div.MuiFormControl-root:nth-child(4) > div:nth-child(2) > div:nth-child(2) > button:nth-child(1)');
  }
  emailValidation () {
    return select ('p.font-bold');
  }
  
  async navigate () {
    await testController.navigateTo(this.url)
  }

  async navigateHome () {
    const getLocation = ClientFunction(() => document.location.href).with({ boundTestRun: testController });
    await testController.expect(getLocation()).contains(process.env.URL_HOST + 'home');
  }

  async register (name, pass) {
    await testController
      .click(this.nameImput())
      .typeText(this.nameImput(),name, { paste: true })
      .click(this.emailImput())
      .typeText(this.emailImput(), email, { paste: true })
      .click(this.passwordImput())
      .typeText(this.passwordImput(), pass, { paste: true })
      .click(this.passViewButton())
      .click(this.passwordValidateImput())
      .typeText(this.passwordValidateImput(), pass, { paste: true })
      .click(this.passValidateViewButton())
      .click(this.registerButton());
  }
  async registerValidation () {
    await testController
      .expect (await this.emailValidation().innerText).eql(email);
  }

  
  async titleSingUp () {
    return select('h6.MuiTypography-root');
  }
}
