import { testController } from '../support/world'
import { select } from '../support/utils'
import { ClientFunction } from 'testcafe';

const password = '123456aA';
const dotenv = require('dotenv');

export class Home {
  constructor () {
    dotenv.config();
    this.url = process.env.URL_HOST + `login`
    this.urlHome = process.env.URL_HOST + `home`
  }

  emailImput  () {
    return select('div.MuiFormControl-root:nth-child(1) > div:nth-child(2) > input:nth-child(1)')
  }

  passwordImput () {
    return select('.pr-2 > input:nth-child(1)')
  }

  loginButton () {
    return select('button.MuiButtonBase-root:nth-child(3)');
  }

  passViewButton () {
    return select('button.MuiButtonBase-root:nth-child(1)')
  }

  userNameNav() {
    return select('p.MuiTypography-root-174:nth-child(1)').innerText
  }

  emailNav() {
    return select('body > div.MuiDrawer-root-446.MuiDrawer-modal-457 > div.MuiPaper-root-460.MuiDrawer-paper-448.jss8.MuiDrawer-paperAnchorLeft-449.MuiPaper-elevation16-479 > div > div > header > p.MuiTypography-root-502.email.text-13.mt-8.opacity-50.whitespace-no-wrap.MuiTypography-body1-504.MuiTypography-colorInherit-524').innerText
  }

  titleNav() {
    return select ('.list-subheader-text').innerText
  }

  homeButtonNav() {
    return select('.makeStyles-item-230')
  }

  gestionUsuariosButtonNav() {
    return select ('a.MuiButtonBase-root-239:nth-child(4)')
  }

  userNameHome() {
    return select('span.MuiTypography-root-87').innerText
  }

  configurationsButtonNav() {
    return select('li.MuiButtonBase-root-239 > div:nth-child(2) > span:nth-child(1)')
  }

  ryderTypesNav() {
    return select ('a.MuiButtonBase-root-239:nth-child(1) > div:nth-child(1) > span:nth-child(1)')
  }

  scheduleNav() {
    return select ('#fuse-navbar > div > div > div > ul > ul > div > div > div')
  }

  gestionSolicitudesButtonNav() {
    return select ('a.MuiButtonBase-root-239:nth-child(5) > div:nth-child(2) > span:nth-child(1)')
  }

  tripsMadeButtonNav() {
    return select ('a.MuiButtonBase-root-239:nth-child(6) > div:nth-child(2) > span:nth-child(1)')
  }

  debtsButtonNav() {
    return select ('a.MuiButtonBase-root-239:nth-child(7) > div:nth-child(2) > span:nth-child(1)')
  }

  reportsNav() {
    return select ('a.MuiButtonBase-root-239:nth-child(8) > div:nth-child(2) > span:nth-child(1)')
  }

  async navigate () {
    await testController.navigateTo(this.url)
  }

  async navigateHome () {
    dotenv.config();
    this.urlHome = process.env.URL_HOST + `apps/home`;
    console.log(this.urlHome);
    const getLocation = ClientFunction(() => document.location.href).with({ boundTestRun: testController });
    await testController.expect(getLocation()).contains(this.urlHome);
  }

  async login (email) {
    await testController
      .click(this.emailImput())
      .typeText(this.emailImput(), email, { paste: true })
      .click(this.passwordImput())
      .typeText(this.passwordImput(), password, { paste: true })
      .click(this.passViewButton())
      .click(this.loginButton())
  }

  async home (email) {
    await testController
      .click(this.configurationsButtonNav())
      .click(this.ryderTypesNav())
      .wait(1500)
      .click(this.scheduleNav())
      .wait(1500)
      .click(this.gestionUsuariosButtonNav())
      .wait(1500)
      .click(this.gestionSolicitudesButtonNav())
      .wait(1500)
      .click(this.tripsMadeButtonNav())
      .wait(1500)
      .click(this.debtsButtonNav())
  }

  async homeValidate (fullName) {
    const nameMenu = this.userNameNav();
    const nameTitle = this.userNameHome();

    await testController
      .expect(nameMenu).eql(fullName)
      .expect(nameTitle).eql(fullName)
  }
}