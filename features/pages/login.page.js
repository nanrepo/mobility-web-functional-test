import { testController } from '../support/world'
import { select } from '../support/utils'
import { ClientFunction } from 'testcafe';

const password = '123456aA';
const dotenv = require('dotenv');

export class Login {
  constructor () {
    dotenv.config();
    this.url = process.env.URL_HOST + `login`
    this.urlHome = process.env.URL_HOST + `home`
  }

  emailImput  () {
    return select('div.MuiFormControl-root:nth-child(1) > div:nth-child(2) > input:nth-child(1)')
  }

  passwordImput () {
    return select('.pr-2 > input:nth-child(1)')
  }

  rubiTitleImg () {
    return select('.mb-32 > img:nth-child(1)').exists
  }
  
  rubiLogoImg () {
    return select ('h3.MuiTypography-root > img:nth-child(1)').exists
  }

  loginTitle () {
    return select ('h6.MuiTypography-root').innerText
  }

  singUpTitle () {
    return select ('span.font-medium').innerText
  }
  singUpText () {
    return select ('a.font-medium:nth-child(2)').innerText
  }

  recoveryPassText () {
    return select ('a.font-medium:nth-child(1)').innerText
  }

  loginButton () {
    return select('button.MuiButtonBase-root:nth-child(3)');
  }

  passViewButton () {
    return select('button.MuiButtonBase-root:nth-child(1)')
  }

  userSettingsButton() {
    return select ('.MuiButtonBase-root-86')
  }

  logoutButton() {
    return select('li.MuiButtonBase-root-86')
  }

  async navigate () {
    await testController.navigateTo(this.url)
  }

  async navigateHome () {
    dotenv.config();
    this.urlHome = process.env.URL_HOST + `apps/home`;
    console.log(this.urlHome);
    const getLocation = ClientFunction(() => document.location.href).with({ boundTestRun: testController });
    await testController.expect(getLocation()).contains(this.urlHome);
  }
  async navigateHomeAndExit () {
    dotenv.config();
    this.urlHome = process.env.URL_HOST + `apps/home`;
    console.log(this.urlHome);
    const getLocation = ClientFunction(() => document.location.href).with({ boundTestRun: testController });
    await testController
    .expect(getLocation()).contains(this.urlHome)
    .click(this.userSettingsButton())
    .click(this.logoutButton())
    .expect(getLocation()).contains(this.url);
  }

  async login (email) {
    await testController
      .click(this.emailImput())
      .typeText(this.emailImput(), email, { paste: true })
      .click(this.passwordImput())
      .typeText(this.passwordImput(), password, { paste: true })
      .click(this.passViewButton())
      .click(this.loginButton())
  }

  async loginValidate (email) {
    const loginTitle = this.loginTitle();
    const rubiTitle = this.rubiTitleImg();
    const rubiLogo = this.rubiLogoImg();
    const singUpTitle = this.singUpTitle();
    const singUpText = this.singUpText();
    const recoveryPassText = this.recoveryPassText();

    await testController
      .expect(rubiTitle).ok('El titulo de Rubi se visualiza', { allowUnawaitedPromise: false })
      .expect(rubiLogo).ok('El logo de Rubi se visualiza', { allowUnawaitedPromise: false })
      .expect(loginTitle).eql("Iniciar sesión")
      .expect(singUpTitle).eql("¿No tienes una cuenta?")
      .expect(singUpText).eql("Registrarse")
      .expect(recoveryPassText).eql("¿Olvidaste tu contraseña?");
  }
}
